function applyQuoteToElement(quote, element) {
  element.querySelector(".scrolling-quote-text").innerText = quote.text;
  element.querySelector(".scrolling-quote-author").innerText = quote.author;
}

class Scroller {
  constructor(quoteData) {
    this.quoteData = quoteData;
    this.numberOfQuotes = quoteData.length;
    const [firstDisplay, secondDisplay] = document.querySelectorAll(
      ".scrolling-quote"
    );
    this.firstDisplay = firstDisplay;
    this.secondDisplay = secondDisplay;
    this.counter = 0;
  }

  updateElements() {
    const firstIndex = this.counter % this.numberOfQuotes;
    const secondIndex = (this.counter + 1) % this.numberOfQuotes;
    applyQuoteToElement(this.quoteData[firstIndex], this.firstDisplay);
    applyQuoteToElement(this.quoteData[secondIndex], this.secondDisplay);
  }

  next() {
    this.counter += 2;
    this.updateElements();
  }
}

async function main() {
  const resp = await fetch("scrolling-quotes.json");
  const quoteData = await resp.json();
  const scroller = new Scroller(quoteData);
  scroller.updateElements();
  setInterval(() => {
    scroller.next();
  }, 7000);
}

main();
